'use strict';

const express = require('express')
const path = require('path')
//Constantes
const PORT = 8081;

// App
const app = express();
app.use(express.static(__dirname));
app.get('/', function(req, res){
  //res.send("Bienvenido a este servidor\n");
    res.sendFile(path.join(__dirname+'/index.html'))
});

// Pagina de inicio
app.get('/landing', function(req, res){

    res.sendFile(path.join(__dirname+'/landing.html'))
});

app.get('/registro', function(req, res){

    res.sendFile(path.join(__dirname+'/src/html/insertarusuario.html'))
});

app.get('/estadisticas', function(req, res){

    res.sendFile(path.join(__dirname+'/src/html/barrad3.html'))
});

app.get('/usuariodet', function(req, res){

    res.sendFile(path.join(__dirname+'/src/html/detalleusuario.html'))
});

app.get('/usuariomod', function(req, res){

    res.sendFile(path.join(__dirname+'/src/html/modificarusuario.html'))
});

app.get('/insertpost', function(req, res){

    res.sendFile(path.join(__dirname+'/src/html/insertarpost.html'))
});

app.get('/modifpost', function(req, res){

    res.sendFile(path.join(__dirname+'/src/html/modificarpost.html'))
});

app.get('/detpost', function(req, res){

    res.sendFile(path.join(__dirname+'/src/html/detallepost.html'))
});

app.get('/buscar', function(req, res){

    res.sendFile(path.join(__dirname+'/src/html/buscar.html'))
});

app.get('/adminpost', function(req, res){

    res.sendFile(path.join(__dirname+'/src/html/consultausu.html'))
});



app.listen(PORT);
console.log('Express funcionado en el puerto' + PORT);
