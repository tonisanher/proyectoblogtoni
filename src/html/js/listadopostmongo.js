//url de mongo
const URLprev = 'https://api.mlab.com/api/1/databases/bootcamp/collections/posts?s={"fecha":-1}';
const apikey = '&apiKey=plZnUksVVShCyccLHHF60bQnVze1kN0s';


var response;
//Valores para paginar de 6 en 6
var postIni = -6;
var postFin = 6;
var paginacion = 6;

//Funcion obtener post sin paginación
function obtenerPosts(opcion){

  var peticion = new XMLHttpRequest();

  var ULR = URLprev + apikey
  peticion.open("GET", URL, false);
  peticion.setRequestHeader("Content-Type","application/json");
  peticion.send();

  if (peticion.status=="200"){
      response = JSON.parse(peticion.responseText);


      if(opcion == "filtro"){
        document.querySelector("consultapostusu-app").listado = JSON.parse(peticion.responseText);
      }
      else {
        document.querySelector("consultapost-app").listado = JSON.parse(peticion.responseText);
      }


      console.log(response);
      //mostrarPosts();
  }
  else
  {
      alert("Casquetazo");
  }
};

//Función de obtener post paginando de 6 en 6
function obtenerPostsPag(opcion,accion){

  var peticion = new XMLHttpRequest();
  console.log("prueba de paginacion");
  if(accion == "adelante"){

    postIni = postIni + paginacion;
    //postFin = postFin + paginacion;
  }
  else{
    if(postIni!=0){
      postIni = postIni - paginacion;
      //postFin = postFin - paginacion;
    }
  }
  console.log("prueba de paginacion");
  var URL = URLprev + '&sk=' + postIni + '&l=' + postFin + apikey;
  console.log(URL);
  peticion.open("GET", URL, false);
  peticion.setRequestHeader("Content-Type","application/json");
  peticion.send();

  if (peticion.status=="200"){
      response = JSON.parse(peticion.responseText);


      if(opcion == "filtro"){
        document.querySelector("consultapostusu-app").listado = JSON.parse(peticion.responseText);
      }
      else {
        document.querySelector("consultapost-app").listado = JSON.parse(peticion.responseText);
      }


      console.log(response);
      //mostrarPosts();
  }
  else
  {
      alert("Casquetazo");
  }
};

//Función que buscar los post asociados al usuario
function buscarPosts(usuario){

  const urlIni = 'https://api.mlab.com/api/1/databases/bootcamp/collections/posts?q={';
  const urlFin = '}&s={"fecha":-1}&apiKey=plZnUksVVShCyccLHHF60bQnVze1kN0s';

  console.log("prueba del usuario"+usuario);
  var UsuarioFil;
  var query = "";
  var filtroFechas = false;

  if (usuario == null || usuario == "" || usuario == undefined)
  {
    UsuarioFil = document.getElementsByName("UsuarioPost")[0].value;
  }
  else {
    var usuarioLog = JSON.parse(sessionStorage["usuariolog"]);
    UsuarioFil = usuarioLog.usuario;
  }


  if(UsuarioFil != null && UsuarioFil != "" && UsuarioFil != undefined)
  {
    query = '"usuario":"'+UsuarioFil+'",';
  }

  var fechaIni = document.getElementById("fechaini").value;
  var fechaFin = document.getElementById("fechafin").value;
  var categoriaPost = document.getElementById("categoriaPost").value;

  if (categoriaPost != "Todas")
  {
      query += '"categoria":"'+categoriaPost+'",';
  }


  var equipoPost = document.getElementById("equipoPost").value;

  if (equipoPost != "Todos")
  {
      query += '"equipo":"'+equipoPost+'",';
  }

  if(fechaIni!="" && fechaFin!="")
  {
      filtroFechas = true;
  }

  console.log(UsuarioFil + "--"+fechaIni+"--"+fechaFin+"--"+categoriaPost+"--"+equipoPost);

  var peticion = new XMLHttpRequest();
  //var urlFiltro = 'http s://api.mlab.com/api/1/databases/bootcamp/collections/posts?q={"usuario":"tonish3","fechafor":"02/09/2017"}&apiKey=plZnUksVVShCyccLHHF60bQnVze1kN0s'
  var urlFiltro = urlIni +query + urlFin

  console.log(urlFiltro);
  peticion.open("GET", urlFiltro, false);


  peticion.setRequestHeader("Content-Type","application/json");
  peticion.send();

  if (peticion.status=="200"){
    console.log(peticion.responseText);
      response = JSON.parse(peticion.responseText);


      if(filtroFechas)
      {
        var postFiltrados = filtrarFechaPost(fechaIni,fechaFin,response);
        document.querySelector("consultapostusu-app").listado = JSON.parse(postFiltrados);

      }
      else
      {
        document.querySelector("consultapostusu-app").listado = JSON.parse(peticion.responseText);

      }

      document.getElementById("btnexportexcel").disable = false;


      //console.log(  document.getElementById("btnexportexcel").disable);
      //mostrarPosts();
  }
  else
  {
      alert("Casquetazo");
  }

};

//Funcion que exporta a Excel
function exportarExcel(usuario,opcion){

  const urlIni = 'https://api.mlab.com/api/1/databases/bootcamp/collections/posts?q={';
  const urlFin = '}&s={"fecha":-1}&apiKey=plZnUksVVShCyccLHHF60bQnVze1kN0s';

  var UsuarioFil;
  var query = "";
  var filtroFechas = false;

  if (usuario == null || usuario == "" || usuario == undefined)
  {
    UsuarioFil = document.getElementsByName("UsuarioPost")[0].value;
  }
  else {
    var usuarioLog = JSON.parse(sessionStorage["usuariolog"]);
    UsuarioFil = usuarioLog.usuario;
  }


  if(UsuarioFil != null && UsuarioFil != "" && UsuarioFil != undefined)
  {
    query = '"usuario":"'+UsuarioFil+'",';
  }

  var fechaIni = document.getElementById("fechaini").value;
  var fechaFin = document.getElementById("fechafin").value;
  var categoriaPost = document.getElementById("categoriaPost").value;

  if (categoriaPost != "Todas")
  {
      query += '"categoria":"'+categoriaPost+'",';
  }


  var equipoPost = document.getElementById("equipoPost").value;

  if (equipoPost != "Todos")
  {
      query += '"equipo":"'+equipoPost+'",';
  }

  if(fechaIni!="" && fechaFin!="")
  {
      filtroFechas = true;
  }

  console.log(UsuarioFil + "--"+fechaIni+"--"+fechaFin+"--"+categoriaPost+"--"+equipoPost);

  var peticion = new XMLHttpRequest();
  //var urlFiltro = 'http s://api.mlab.com/api/1/databases/bootcamp/collections/posts?q={"usuario":"tonish3","fechafor":"02/09/2017"}&apiKey=plZnUksVVShCyccLHHF60bQnVze1kN0s'
  var urlFiltro = urlIni +query + urlFin

  console.log(urlFiltro);
  peticion.open("GET", urlFiltro, false);


  peticion.setRequestHeader("Content-Type","application/json");
  peticion.send();

  if (peticion.status=="200"){
    console.log(peticion.responseText);
      response = JSON.parse(peticion.responseText);


      if(filtroFechas)
      {
        var postFiltrados = filtrarFechaPost(fechaIni,fechaFin,response);
        JSONToCSVConvertor(postFiltrados,"fichero");
      }
      else
      {
          JSONToCSVConvertor(peticion.responseText,"fichero");
      }



      console.log("rapido"+response[0]);
      //mostrarPosts();
  }
  else
  {
      alert("Casquetazo");
  }

};

//Funcion que filtra el json por fechas
function filtrarFechaPost(fechaIni,fechaFin,response){

  console.log(fechaIni+"  "+fechaFin)
  var fechaPost;
  var postFil = new Array();
  var retornoFil= "";

  if(response!=undefined)
  {
    for (var i = 0; i < response.length; i++) {

      if(response[i].fecha != undefined)
      {
        fechaPost = response[i].fecha.$date
        fechaPost = fechaPost.slice(0,10);
        console.log("la recupero"+fechaPost);

        if (fechaPost>=fechaIni && fechaPost <= fechaFin){
          postFil.push(response[i]);
          console.log("CUMPLE"+fechaPost);
        }
      }
    }
    retorno = JSON.stringify(postFil);

  }

  console.log("a ver"+retorno);

  return retorno;

};

//Funcion que genera el fichero excel
function JSONToCSVConvertor(JSONData, nomFichero) {

    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
    var cabecera = "";
    var CSV = '';

    CSV += nomFichero + '\r\n\n';
    cabecera = "Título;Texto;Categoria;Equipo;Fecha de Alta;Usuario";
    CSV += cabecera + '\r\n';

    for (var i = 0; i < arrData.length; i++) {
        var row = "";
        console.log("indice 1:"+i);
        for (var index in arrData[i]) {

            if(index == "titulo" ||index == "texto"||index == "categoria"||index == "equipo" ||index == "fechafor" ||index == "usuario")
            {
              row += '"' + arrData[i][index] + '";';
            }
        }

        row.slice(0, row.length - 1);
        CSV += row + '\r\n';
    }

    if (CSV == '') {
        alert("Invalid data");
        return;
    }

    var fileName = "Reporte_";
    fileName += nomFichero.replace(/ /g,"_");

    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

    var link = document.createElement("a");
    link.href = uri;

    link.style = "visibility:hidden";
    link.download = fileName + ".csv";

    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}
