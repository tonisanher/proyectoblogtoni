// Función que recupera el usuario de la sesión y lo vuelca a la barra
// En el caso de que no haya usuario en sesión, se redirige automaticamente a login
function cargarUsuario()
{

  var usuarioSession = sessionStorage["usuariolog"];

  if ( usuarioSession == undefined)
  {
      location.href="/";
  }
  else
  {
  var usuarioLog = JSON.parse(sessionStorage["usuariolog"]);

  var usuario = usuarioLog.usuario;
  var equipo = usuarioLog.equipo;
  var imagen = usuarioLog.imagen;

  var spanUsuario = document.getElementById("nombreLogado");
  //console.log("jjjj"+usuario+"dd");

      if(usuarioLog.autor != undefined)
      {
          document.getElementById("nombrebarra").innerHTML = "<strong>" + usuarioLog.autor.nombre + " " + usuarioLog.autor.apellido +"<strong>";
      }
      else
      {
          spanUsuario = "Sin nombre";
      }



      document.getElementById("usuariobarra").innerHTML ='<strong>' + usuario + "<strong>";
      document.getElementById("equipobarra").innerHTML = equipo;
  }

}
  //Función que limpia la sessionStorage y redirige a la página de login
  function realizarlogout(){
    sessionStorage.clear();
    location.href="/";

  }
