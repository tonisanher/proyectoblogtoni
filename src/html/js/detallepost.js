
//Datos de la URL
const urlBBBDD = "https://api.mlab.com/api/1/databases/bootcamp/collections/posts"
const apiKey = "plZnUksVVShCyccLHHF60bQnVze1kN0s";

//Variable de respuesta
var response;


// Función para recuperar el post almacenado en la sesion consultando por
// el id en la BBDD mongo
function recuperarPost()
{
  const cteNoEncontrado = "Post no encontrado";

  var idPost = sessionStorage["postDetallar"];

  if ((idPost != "") && (idPost != undefined))
  {
      var urlId = urlBBBDD + "/" + idPost + "?apiKey=" + apiKey;

      var peticion = new XMLHttpRequest();
      peticion.open("GET", urlId, false);
      peticion.setRequestHeader("Content-Type","application/json");
      peticion.send();

      if (peticion.status=="200")
      {
          response = JSON.parse(peticion.responseText);
          rellenarPost();
      }
      else
      {
          document.getElementById("dtllePost").innerHTML = cteNoEncontrado;
      }
  }
  else
  {
      document.getElementById("dtllePost").innerHTML = cteNoEncontrado;
  }

}

// Funcion para añadir los valores obtenidos de la consulta de la BBDD
// al html

function rellenarPost()
{
  document.querySelector("comentarios-app").idpost = sessionStorage["postDetallar"];
  console.log("el post a detalltar:"+sessionStorage["postDetallar"])

  var tituloPost = response.titulo;

  if ((tituloPost != "") && (tituloPost != undefined))
  {
    document.getElementById("titPost").innerHTML = tituloPost;
  }
  else
  {
    document.getElementById("titPost").innerHTML = "Sin titulo";
  }

  var textoPost = response.texto;

  if ((textoPost != "") && (textoPost != undefined))
  {

    var textoFor = textoPost.replace(/\r?\n/g, "<br>");
    //document.getElementById("textPost").innerHTML = "<pre>" + textoPost+"</pre>";
    document.getElementById("textPost").innerHTML = textoFor;
  }
  else
  {
    document.getElementById("textPost").innerHTML ="Sin texto";
  }

/*
if ((response.autor != undefined))
  {
    var nombre = response.autor.nombre;

    var apellidos = response.autor.apellido;

    if ((apellidos != "") && (apellidos != undefined))
    {
      var nombreCompleto = nombre + " " + apellidos;
    }
    else
    {
      var nombreCompleto = nombre;
    }

    document.getElementById("autorPost").innerHTML = nombreCompleto;

  }
  else
  {
    document.getElementById("autorPost").innerHTML = "Sin autor";
  }
  */

  var categoriaPost = response.categoria;

  if (response.categoria != undefined)
  {
    document.getElementById("categoriaPost").innerHTML = response.categoria;
  }
  else
  {
    document.getElementById("categoriaPost").innerHTML = "Sin categoria";
  }

  var equipoPost = response.equipo;

  if (response.equipo != undefined)
  {
    document.getElementById("equipoPost").innerHTML = equipoPost;
  }
  else
  {
    document.getElementById("equipoPost").innerHTML = "Sin equipo";
  }

  if (response.rutadoc != undefined)
  {
    document.getElementById("rutadoc").href = response.rutadoc;
  }
  else
  {
    document.getElementById("rutadoc").innerHTML = "";
  }
  console.log("ddddddddddddddddd");

  if (response.fechafor != undefined)
  {
    document.getElementById("fechaPost").innerHTML = response.fechafor;

  }
  else
  {
    document.getElementById("fechaPost").innerHTML = "Sin fecha";
  }
  console.log("eeeeeeeeeeeeeeeeeeeeeee"+response.usuario);

  if (response.usuario != undefined)
  {
    document.getElementById("usurioPost").innerHTML = response.usuario;
    console.log("fff");
  }
  else
  {
    document.getElementById("usurioPost").innerHTML = "Sin Usuario";
  }
  console.log("zzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
  if (response.imagen != undefined)
  {


    var imagenHtml = document.getElementById("imagenPost");

    if (response.imagen != "")
    {
        imagenHtml.src = response.imagen;
    }
    else
    {
        imagenHtml.src = "http://www.wimacpc.com/thumbs/314x0/assets/images/no-disponible.png";
    }
    console.log("zzzz"+imagenHtml.src);
  }
  else
  {
    document.getElementById("imagenPost").src = "http://www.wimacpc.com/thumbs/314x0/assets/images/no-disponible.png";
  }
  console.log("qqqqqqqqqqqqqqqqqqqq");
  /*if (response.fecha != undefined)
  {
    var fechaPost = new Date(response.fecha.$date);

    var dia = fechaPost.getDate();
    var mes = fechaPost.getMonth();
    mes += 1;
    var anyo = fechaPost.getFullYear();

    document.getElementById("fechaPost").innerHTML = dia + "/" + mes + "/" + anyo;
  }*/

  /*if (response.votoPos != undefined)
  {
    document.getElementById("votoPosPost").innerHTML = response.votoPos;
  }
  else
  {
    document.getElementById("votoPosPost").innerHTML = 0;
  }

  if (response.votoNeg != undefined)
  {
    document.getElementById("votoNegPost").innerHTML = response.votoNeg;
  }
  else
  {
    document.getElementById("votoNegPost").innerHTML = 0;
  }*/



}
