
//Datos de la URL
const urlBBBDD = "https://api.mlab.com/api/1/databases/bootcamp/collections/usuarios"
const apiKey = "plZnUksVVShCyccLHHF60bQnVze1kN0s";

//Variable de respuesta
var response;


// Función para recuperar el Usuario almacenado en la sesion consultando por
// el id en la BBDD mongo
function recuperarUsuario()
{
  const cteNoEncontrado = "Usuario no encontrado";

  usuarioLog = JSON.parse(sessionStorage["usuariolog"]);

  IdUsu = usuarioLog.idmongo;

  if ((usuarioLog != "") && (usuarioLog != undefined) && (usuarioLog != null))
  {
      var urlId = urlBBBDD + "/" + IdUsu + "?apiKey=" + apiKey;

      var peticion = new XMLHttpRequest();
      peticion.open("GET", urlId, false);
      peticion.setRequestHeader("Content-Type","application/json");
      peticion.send();

      if (peticion.status=="200")
      {
          response = JSON.parse(peticion.responseText);
          rellenarUsuario(usuarioLog);
      }
      else
      {
          document.getElementById("detalleUsuario").innerHTML = cteNoEncontrado;
      }
  }
  else
  {
      document.getElementById("detalleUsuario").innerHTML = cteNoEncontrado;
  }

}

// Funcion para añadir los valores obtenidos de la consulta de la BBDD
// al html

function rellenarUsuario(usuarioLog)
{

  var username = response.username;

  if ((username != "") && (username != undefined))
  {
    document.getElementById("username").innerHTML = username;
  }
  else
  {
    document.getElementById("username").innerHTML = "Sin username";
  }

  var email = response.email;

  if ((email != "") && (email != undefined))
  {
    document.getElementById("email").innerHTML = email;
  }
  else
  {
    document.getElementById("email").innerHTML ="Sin email";
  }


 if ((response.usuario != undefined))
  {
    var nombre = response.usuario.nombre;

    var apellidos = response.usuario.apellido;

    if ((apellidos != "") && (apellidos != undefined))
    {
      var nombreCompleto = nombre + " " + apellidos;
    }
    else
    {
      var nombreCompleto = nombre;
    }

    document.getElementById("nombrecompleto").innerHTML = nombreCompleto;

  }
  else
  {
    document.getElementById("autorPost").innerHTML = "Sin nombre";
  }

  var equipo = response.equipo;

  if (response.equipo != undefined)
  {
    document.getElementById("equipo").innerHTML = equipo;
  }
  else
  {
    document.getElementById("equipo").innerHTML = "Sin equipo";
  }

  if (response.telefono != undefined)
  {
    document.getElementById("telefono").innerHTML = response.telefono;
  }
  else
  {
    document.getElementById("telefono").innerHTML = "Sin Teléfono";
  }

  if (response.ciudad != undefined)
  {
    document.getElementById("ciudad").innerHTML = response.ciudad;
  }
  else
  {
    document.getElementById("ciudad").innerHTML = "Sin Ciudad";
  }


  if (response.imagen != undefined)
  {

    var imagenHtml = document.getElementById("imagen");

    if (response.imagen!="")
    {
      imagenHtml.src = response.imagen;
    }
    else
    {
      imagenHtml.src = "http://efectococuyo.com/wp-content/uploads/2015/04/Sin-usuario.jpg";
    }


  }
  else
  {
    imagenHtml.src = "http://efectococuyo.com/wp-content/uploads/2015/04/Sin-usuario.jpg";
  }

}

function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 8,
    center: {lat: -34.397, lng: 150.644}
  });
  console.log("sdfsdfdsfdfd");
  var geocoder = new google.maps.Geocoder();
  geocodeAddress(geocoder, map);
//  document.getElementById('submit').addEventListener('click', function() {

//  });
}

function geocodeAddress(geocoder, resultsMap) {
  //var address = document.getElementById('address').value;
  usuarioLog = JSON.parse(sessionStorage["usuariolog"]);

  //var direccion = JSON.stringify("{'address': '" + usuarioLog.ciudad + '"}');
  direccion = usuarioLog.ciudad;
  console.log(direccion);
  geocoder.geocode({'address': direccion}, function(results, status) {
  //  geocoder.geocode(direccion, function(results, status) {
    if (status === 'OK') {
      resultsMap.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
        map: resultsMap,
        position: results[0].geometry.location
      });
    } else {
      alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}
